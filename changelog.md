# Changelog
I'll catalog all notable changes to this resource pack in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Subfolder structure to enable Optifine's CTM
- Random CTM for full brick block
- Stretcher brick arch on obsidian
- Header brick arch on diamond_block
- Bleached vertical wood planks on oak_planks
- Gray framed glass panes on light_gray_stained_glass
- Black framed glass panes on black_stained_glass
- License file in the root directory
- Toggleable single block shutter to oak_trapdoor, hinges always left
- Beam brace on the opened dark_oak_fence_gate
- Different brick pattern on all blocks exposed to a non-brick block above.
- Different types of slopes for testing roofs
- Toggleable single block shutter to oak_trapdoor, hinges right, top and bottom using all available blockstates
- Diagonal, Arched and Single Square veneers on oak_button using all available blockstates.
- Roof tile CTM on polished_blackstone
- Roof tip custom model and textures on red_sandstone_wall
- Thatch CTM on polished_blackstone_bricks
- Slate CTM on blackstone
- Clay plaster on yellow_terracotta
- Prototype timber frame CTM on yellow_concrete
- Prototype timber frame crosshatch and diagonal hatches on stripped_oak_log
- Small pink sandstone on polished_granite
- Medium pink sandstone on granite
- Large pink sandstone on diorite
- Dust CTM for all three pink sandstone variants
- Random CTM for dirt
- Random CTM for grass_block
- Overlay CTM for grass_block
- Overlay CTM for dirt
- Random CTM for stone
- Denser model for grass plant

### Changed

### Removed